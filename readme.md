# Refinamento dos rótulos

Refinamento dos rótulos na base de dados já atualizada com o acervo do museu da abolição, contabilizando assim 17 museus. Como a página de integração do Tainacan ainda está em desenvolvimento possui muita instabilidade, então peguei diretamente dos acervos de cada museu.

## Stage 3
Com a verificação dos metadados título, material, denominação e técnica constatamos que denominação e título são os que mais seguem um padrão e possuem maior generalização entre os objetos. Sendo assim, foram analisada as frequências entre esses dois tipos.

Através de uma comparação direta entre os dois tipos foi possível verificar que os mesmos dividem em grande parte os rótulos mais frequentes, embora com frequências diferentes.

Se tomarmos como base denominação e compararmos ambas as colunas de frequência podemos verificar que os dados de denominação encontram-se dentro dos 30 principais rótulos de título, com a remoção de vaso e lampião. Sendo assim, decidimos manter vaso e lampião na base de dados, tendo em mente que os mesmos são os mais próximos dos top 30 e que aparecem em pelo menos uma das listas.
as
## Stage 2
Contem as palavras retiradas dos labels títulos e denominação. Esta análise foi obtida através de filtro (stop words) aplicado nos dados obtidos no stage 1. Ambos labels possuem palavras em comum, então muitas palavras que foram removidas de um grupo atingiram o outro, porém foi verificado se fazia sentido a remoção.

**Imagens:** threshold efetuado com as trinta palavras que possuem maior frequência na base. \
**Jsons:** possuem todos os dados com suas respectivas frequências, com exceção as stop words.

_Obs.:_ Esta pasta foi adicionada em 24-08-21, não possui ainda considerações.

## Stage 1
### Cenários

> 0: sem filtro de STOP WORDS. \
> 1: com filtro de STOP WORDS. \
> A: sem colocação de palavras juntas (bigram). \
> B: com colocação de palavras juntas (bigram).

Cenário 1: 0A
- Sem filtro de STOP WORDS e sem colocação de palavras juntas.

Cenário 2: 0B
-	Sem filtro de STOP WORDS e com colocação de palavras juntas.

Cenário 3: 1A
-	Com filtro de STOP WORDS e sem colocação de palavras juntas.
- Utilizado para a criação da classe 1 com uma única palavra.

Cenário 4: 1B
-	Com filtro de STOP WORDS e com colocação de palavras juntas.
- Utilizado para a criação da classe 2 com duas palavras

### Pastas:
#### wordcloud-images
Imagens criadas das nuvens de palavras de título, técnica, material e denominação.

#### refined-label-txt
Arquivos txt para cada uma das categorias, título, técnica, material e denominação.

#### raw-frequency
Tabela das 20 maiores frequências de cada rótulo para cada um dos cenários. O cenário pode ser identificado pelo nome do arquivo.


## Considerações
Em todos os items foi efetuado o refinamento na base completa, não havendo corte pela frequência que as palavras aparecem.

As nuvens de palavras aparecem diferente dos arquivos txt devido os arquivos txt terem sido construídos sobre as nuvens de palavras, ou seja, foi construido a nuvem e com o resultado da mesma efetuado o refinamento.

##### Título
- Foi possível obter refinamento em duas classes, uma contendo apenas uma palavra (arquivo classe 1) e outro contendo mais que uma (arquivo classe 2). Um ponto que fiquei em dúvida é com relação a palavra XIX ou a junção Séc. XIX. Devemos inserir?

##### Materiais
- Neste caso existem muitas palavras que aparecem juntas devido o item ter sido construído por diversos tipos de material, por exemplo, item de porcela, com pó de ouro e tinta, tendo neste caso as palavras Porcelana, Pó de Ouro e Tinta juntas, o que produz  uma má interpretação nas nuvens de palavras que efetuam junção de palavras (cenários com final B).
- Possuia items que não são materiais, como por exemplo marcenaria, colagem. Estes mais parecem ser técnicas.

##### Técnica
- Este tipo de rótulo possui em sua maioria somente uma palavra por descrição, podendo um objeto ter mais que uma palavra. Por exemplo, cadeira: Madeira | Tinta | Entalhada. Por este motivo grande parte da junção de palavras aparecem com rótulos repetidos. Talvez seja melhor utilizar esta categoria com somente uma palavra (arquivo txt de classe 1 ou cenários final A).

##### Denominação
- Este aquivo é bem extenso, possui muita informação. Ele se assemelha ao título, porém com descrições breves.
- Ele produz uma grande quantidade de palavras. Ambos os arquivos txt de classe 1 e 2 são bem interessantes, embora o segundo de dificil construção pela quantidade de palavras.
- Algumas palavras foram retiradas. Um bom exemplo é "luminária fragmento", pois são muitas as partes da luminária, não sendo possível muitas vezes a identificação de uma luminária como objeto.

